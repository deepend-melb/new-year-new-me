const fs = require("fs");
const path = require("path");
const config = require("../src/config");
const { DynamoDB } = require("aws-sdk");

const usersHelper = () => {
  return new Promise(resolve => {
    const client = new DynamoDB.DocumentClient({ region: "ap-southeast-2" });
    client.scan({ TableName: config.dynamoDB.tables.users }, (err, data) => {
      if (err) {
        console.log(err);
      }
      resolve(data.Items);
    });
  });
};

module.exports = usersHelper;
