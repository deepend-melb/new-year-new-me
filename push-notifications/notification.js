const { google } = require("googleapis");
const request = require("request");
const key = require("./key.json");
const usersHelper = require("./get-all-users");

// send the notifications
const sendNotification = users => {
  const jwtClient = new google.auth.JWT(
    key.client_email,
    null,
    key.private_key,
    ["https://www.googleapis.com/auth/actions.fulfillment.conversation"],
    null
  );

  jwtClient.authorize((err, tokens) => {
    users.forEach(user => {
      const customPushMessage = {
        userNotification: {
          title: "Resolution Check In",
        },
        target: {
          userId: user.token,
          intent: "ResolutionCheckIn",
        },
      };
      if (!err) {
        request.post(
          "https://actions.googleapis.com/v2/conversations:send",
          {
            auth: {
              bearer: tokens.access_token,
            },
            json: true,
            body: {
              customPushMessage,
            },
          },
          (err, httpResponse, body) => {
            console.log(
              `${httpResponse.statusCode}: ${
                httpResponse.statusMessage
              } ${JSON.stringify(body)} ${user.token}`
            );
          }
        );
      }
    });
  });
};

// get all the users and then send them their check in notification
const getallUsersAndSendNotifications = async () => {
  const allUsers = await usersHelper()

  const usersWithNotifications = allUsers.filter(
  user => user.token && user.notification
  );
  sendNotification(usersWithNotifications);
}
  // getallUsersAndSendNotifications()

  exports.handler = (event, context, callback) => {
    getallUsersAndSendNotifications();
    return true;
  };
