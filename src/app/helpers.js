const chooseResolutionNumber = lengthOfArray => {
  return Math.floor(Math.random() * lengthOfArray) + 1;
};

const removeEmojis = string => {
  return string.replace(/(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])/g, "");
};

module.exports = {
  chooseResolutionNumber,
  removeEmojis,
};
