const {
  AlexaPlatform,
  GoogleAssistantPlatform,
  plugins,
  VoxaApp
} = require("voxa");
const voxaDashbot = require("voxa-dashbot").register;
const config = require("../config");
const Model = require("./model");
const User = require("../services/User");
const states = require("./states");
const variables = require("./variables");
const views = require("./views.json");
const { removeEmojis } = require('./helpers')

let environment = process.env.NODE_ENV || "staging";

if (environment === "local.example") {
  environment = "staging";
}

// const defaultFulfillIntents = require(`../content/${environment}-canfulfill-intents.json`);

const voxaApp = new VoxaApp({ Model, views, variables });
states(voxaApp);

exports.alexaSkill = new AlexaPlatform(voxaApp);
exports.assistantAction = new GoogleAssistantPlatform(voxaApp);

plugins.replaceIntent(voxaApp);
voxaDashbot(voxaApp, config.dashbot);

/**
 * Load User into the model
 */
voxaApp.onRequestStarted(async (voxaEvent) => {
  console.log(voxaEvent.dialogflow.conv.intent)
  const user = await User.get(voxaEvent);

  voxaEvent.model.user = user;
});

/**
 * Update the session count
 */
voxaApp.onSessionStarted(async (voxaEvent) => {
  const user = voxaEvent.model.user;

  user.newSession();
});


voxaApp.onUnhandledState((voxaEvent) => {
  // log.info('unhandled', voxaEvent.intent.name);
  return { to: 'FallbackState'}
});

voxaApp.onAfterStateChanged((voxaEvent, reply, transition) => {
  const state = transition.to
  if (state !== "RepeatState" && state !== "entry" && state !== "FallbackState") {
    voxaEvent.model.state = state;
  }
})

/**
 * Save the user
 */
voxaApp.onBeforeReplySent(async (voxaEvent, reply) => {
    const user = voxaEvent.model.user;
    // get the response and remove emojis from the speech.
    if (reply.payload.google.richResponse) {
      const speechResponse = removeEmojis(reply.payload.google.richResponse.items[0].simpleResponse.textToSpeech)
      reply.payload.google.richResponse.items[0].simpleResponse.textToSpeech = speechResponse
      voxaEvent.model.response = speechResponse
    }
    await user.save({ userId: voxaEvent.user.userId });
  }
);

exports.voxaApp = voxaApp;