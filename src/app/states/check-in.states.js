const User = require("../../services/User");
function register(voxaApp) {

  voxaApp.onState('CheckInUserorNewResolution', async (voxaEvent) => {
    const response = voxaEvent.intent.name

    switch (response) {
      case "CheckInUserIntent":
        return { to: "ResolutionCheckIn" }
      case "NewResolutionIntent":
        return { to: "ChangeResolution" }
    }
    
  })

  voxaApp.onState('ResolutionCheckIn', async (voxaEvent) => {
    const user = voxaEvent.model.user;
    const checkInResponse = user.data.checkInResponse;

      if (checkInResponse === undefined) {
        return { reply: "ResolutionCheckIn", to: "CheckInResponse" }
      } else  {
        switch (checkInResponse) {
          case "Negative":
            return { reply: "PreviousNegative.ResolutionCheckIn", to: "CheckInResponse" }
          case "Neutral":
            return { reply: "PreviousNeutral.ResolutionCheckIn", to: "CheckInResponse" }
          case "Positive":
            return { reply: "PreviousPositive.ResolutionCheckIn", to: "CheckInResponse" }  
        }
      } 
  })

  voxaApp.onState('CheckInResponse', async (voxaEvent) => {
    const response = voxaEvent.intent.name
    const user = voxaEvent.model.user;
    switch (response) {
      case "CheckInNegativeIntent":
        user.saveCheckIn("Negative");
        return { reply: "NegativeCheckIn", to: "exit"}
      case "CheckInNeutralIntent":
        user.saveCheckIn("Neutral");
        return { reply: "NeutralCheckIn", to: "exit"}
      case "CheckInPositiveIntent":
        user.saveCheckIn("Positive");
        return { reply: "PositiveCheckIn", to: "exit"}
    }
})

// double check a returning user wants to change their resolution.
voxaApp.onState('ChangeResolution', async (voxaEvent) => {
  return { reply: "Returning.ChangeResolution", to: "NewResolutionConfirm" }
})

voxaApp.onState('NewResolutionConfirm', async (voxaEvent) => {
  const response = voxaEvent.intent.name
  switch (response) {
    case 'YesIntent':
    case 'GiveMeAResolutionIntent':
      return { to: "ResolutionState" }
    case "NoIntent":
      return { reply: "NoResolutionChange", to: "NoChange" }
  }
})

voxaApp.onState('NoChange', async (voxaEvent) => {
  const response = voxaEvent.intent.name
  switch (response) {
    case 'YesIntent':
      return { to: "ResolutionCheckIn" }
    case "NoIntent":
      return { reply: "Exit.General", to: "exit" }
  }
})


}

module.exports = register;
