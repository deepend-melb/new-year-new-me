function register(voxaApp) {

  voxaApp.onIntent('HelpIntent', voxaEvent => {
    const user = voxaEvent.model.user;
    if (user.data.resolution) {
      return { reply: 'Help.WithResolution', to: 'LaunchState' };
    }
    return { reply: 'Help.WithoutResolution', to: 'LaunchState' };
  });

  voxaApp.onIntent('RepeatIntent', () => ({ to: 'RepeatState' }));
  voxaApp.onState('RepeatState', voxaEvent => {
    const to = voxaEvent.model.state
    return { reply: "Repeat.General", to }
  });

  voxaApp.onState('FallbackState', voxaEvent => {
    const to = voxaEvent.model.state
    if (!voxaEvent.model.unhandledEvents) {
      voxaEvent.model.unhandledEvents = 1
    } else {
      voxaEvent.model.unhandledEvents += 1
    }
    if (voxaEvent.model.unhandledEvents === 3) {
      return { reply: 'Resolution.Unhandled', to: 'exit' };
    }

    return { reply: "Misheard", to };
  });

  voxaApp.onIntent('StopIntent', async (voxaEvent) => {
    return { to: "exit" };
  })

  voxaApp.onState('CancelIntent', async (voxaEvent) => {
    return { to: "exit" };
  })

  voxaApp.onState('exit', async (voxaEvent) => {
    return { reply: "Exit.General", to: "die" };
  })
}

module.exports = register