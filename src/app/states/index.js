const mainStates = require("./main.states");
const notificationStates = require("./notification.states")
const checkInStates = require("./check-in.states")
const generalStates = require("./general.states")
function register(voxaApp) {
  mainStates(voxaApp);
  notificationStates(voxaApp);
  checkInStates(voxaApp);
  generalStates(voxaApp);
}

module.exports = register;
