const cards = require('../../content/en-AU/resolution-cards.json')
const { Image } = require('actions-on-google')
const { chooseResolutionNumber } = require('../helpers')
const User = require("../../services/User");

let chosenResolution;
let resolutionOptions = cards;

function register(voxaApp) {

  // go to launch state when action is invoked
  voxaApp.onIntent('LaunchIntent', () => ({ to: 'LaunchState' }));

  // go to launch state when push notification is pressed.
  voxaApp.onIntent('ResolutionCheckIn', () => ({ to: 'LaunchState' }));

  voxaApp.onState('LaunchState', async (voxaEvent) => {
    const sessions = voxaEvent.model.user.sessionCount;
    const verificationStatus = voxaEvent.rawEvent.originalDetectIntentRequest.payload.user.userVerificationStatus;
   
    if (verificationStatus === 'GUEST') {
      return { reply: "UnverifiedUser", to: "exit" }
    }

    if (sessions < 2) {
      return { reply: "FirstTimeUser", to: "ResolutionConfirm" }
    } else {
      if (!voxaEvent.model.user.data.resolution) {
        return { reply: "Returning.NoResolution", to: "ResolutionConfirm" }
      } else {
        return { reply: "ReturningUserConfirmedResolution", to: "CheckInUserorNewResolution" }
      }
    }
  })

  voxaApp.onState('ResolutionConfirm', async (voxaEvent) => {
      const response = voxaEvent.intent.name
      switch (response) {
        case 'YesIntent':
        case 'GiveMeAResolutionIntent':
          return { to: "ResolutionState" }
        case "NoIntent":
          return { reply: "NoResolution", to: "DoubleCheck" }
      }
  })

  voxaApp.onState('DoubleCheck', async (voxaEvent) => {
    const response = voxaEvent.intent.name

    switch (response) {
      case "YesIntent":
        return { reply: "NoResolutionExit", to: "exit" }
      case "NoIntent":
        return { reply: "YesResolution", to: "ResolutionState" }
    }
  })

  voxaApp.onState('ResolutionState', async (voxaEvent) => {
        return { reply: "Resolution", to: "ResolutionOption" }
  })

  voxaApp.onState('ResolutionOption', async (voxaEvent) => {
      const user = voxaEvent.model.user;
      // if a user is returning and have a resolution don't repeat it when they want to get a new one
      if(user.data.resolution && resolutionOptions.length > 9) {
        console.log("here");
        
        resolutionOptions = resolutionOptions.filter(option => option.ID !== user.data.resolution.ID)
      }
      let num = chooseResolutionNumber(resolutionOptions.length)
      
      const resCard = resolutionOptions[num - 1]
      chosenResolution = resCard
      // remove the resolutions that have been told
      resolutionOptions = resolutionOptions.filter(option => option.ID !== resCard.ID);
      console.log(resolutionOptions.length)
      return {
      reply: `FirstTimeResolution${resCard.ID}`,
      dialogflowBasicCard: {
        text: resCard.text,
        subtitle: resCard.subtitle,
        title: resCard.title,
        image: new Image({
          url: resCard.url,
          alt: resCard.alt,
        }),
      },
      to: "ResolutionCheck" 
    }
  })

  voxaApp.onState('ResolutionCheck', voxaEvent => {
    const response = voxaEvent.intent.name
  
    switch (response) {
      case "YesIntent":
        return { reply: "ResolutionSave", to: "SaveResolution"}
      case "LikeThisResolutionIntent":
      return { reply: "ResolutionSave", to: "SaveResolution"}
      case "AnotherIntent":
        if (resolutionOptions.length === 0) {
          resolutionOptions = cards
          return { reply: "ResolutionStartAgain", to: "ResolutionOption"}
        }
        return { reply: "ResolutionRefresh", to: "ResolutionOption"}
    }
  })

  voxaApp.onState("SaveResolution", voxaEvent => {
    const user = voxaEvent.model.user;
    user.saveResolution(chosenResolution);
    
    if (voxaEvent.dialogflow.conv.screen === false) {
      return { reply: "NoScreen", to: "exit"};
    }

    if (user.data.notification) {
      return { reply: "Notify.Setup", to: "exit"};
    }

    return { reply: "ResolutionDetails", to: "ConfirmPushNotifications"};
  });

}

module.exports = register;
 