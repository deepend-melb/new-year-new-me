const {
  dialogflow,
  UpdatePermission,
  Suggestions,
} = require("actions-on-google");

const User = require("../../services/User")

function register(voxaApp) {
  voxaApp.onState("ConfirmPushNotifications", voxaEvent => {
    const response = voxaEvent.intent.name;
    switch (response) {
      case "YesIntent":
        return {
          dialogflowUpdatePermission: {
            intent: "ResolutionCheckIn",
          }
        };
      case "NoIntent":
        voxaEvent.model.user.data.notification = false
        return { reply: "Notify.No", to: "exit" };
    }
  });

  voxaApp.onIntent("FinishPushNotificationsSetupIntent", async (voxaEvent) => {
    if (voxaEvent.intent.params.PERMISSION) {
      const token = voxaEvent.intent.params.UPDATES_USER_ID;
      voxaEvent.model.user.data.token = token
      voxaEvent.model.user.data.notification = true
      return { reply: "Notify.Yes" }
    } else {
      voxaEvent.model.user.data.notification = false
      return { reply: "Notify.No"}
    }
  });
}
module.exports = register;
