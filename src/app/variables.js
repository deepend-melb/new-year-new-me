exports.name = function(voxaEvent) {
  return voxaEvent.model.name;
};

exports.currentResolution = function(voxaEvent) {
  return voxaEvent.model.user.data.resolution.reminder;
};

exports.resolutionTip = function(voxaEvent) {
  return voxaEvent.model.user.data.resolution.tip;
};

exports.lastResponse = function(voxaEvent) {
  return voxaEvent.model.response;
};

exports.fireworks = () =>
  '<audio src= "https://nynm-bucket.s3.amazonaws.com/fireworks.wav" />';
