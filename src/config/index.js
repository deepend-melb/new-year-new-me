const AWS = require('aws-sdk');
const path = require('path');

const getEnv = () => process.env.NODE_ENV || process.env.STAGE || 'local';

const configFile = !process.env.AWS_LAMBDA_FUNCTION_NAME
  ? require(path.join(__dirname, `${getEnv()}.json`))
  : require(path.resolve(
    process.env.LAMBDA_TASK_ROOT,
    '_optimize',
    process.env.AWS_LAMBDA_FUNCTION_NAME,
    `src/config/${getEnv()}.json`,
  ));
AWS.config.update(configFile.aws);

module.exports = configFile;
module.exports.asFunction = () => configFile;
